#include<stdio.h>
#define N 100

int main() {
    int v[N][N], n, m, i, j, elemente = 0, suma = 0;
    printf("Numar de linii: ");
    scanf("%d", &n);
    printf("Numar de coloane: ");
    scanf("%d", &m);
    elemente = n*m;
    printf("Introduceti %d numere\n", elemente);
    for (i = 0; i < n; i ++){
        for (j = 0; j < m; j++){
            scanf("%d", &v[i][j]);
        }
    }
    // calculam suma de pe laturile de sus si jos//
    for (i = 0;i < m; i++){
        suma = suma + v[0][i];
        suma = suma + v[n-1][i];
    }
    //adaugam suma de pe laturile de la stanga si de la dreapta fara numerele de intersectie a laturilor//
    for (i = 1; i < n-1; i++){
        suma = suma + v[i][0];
        suma = suma + v[i][m-1];
    }
    printf("Suma elementelor de pe marginea matricii: %d\n", suma);
    return 0;
}

