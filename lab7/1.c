#include<stdio.h>
#define N 100

int main() {
    int v[N][N], n, m, i, j, max = 0, nr = 0, linia = 0, elemente = 0;
    printf("Numar de linii: ");
    scanf("%d", &n);
    printf("Numar de coloane: ");
    scanf("%d", &m);
    elemente = n*m;
    printf("Introduceti %d numere\n", elemente);
    for (i = 0; i < n; i ++){
        for (j = 0; j < m; j++){
            scanf("%d", &v[i][j]);
        }
    }
    //aflam maximul de numere nenule de pe o linie//
    max = 0;
    for (i = 0; i < n; i++){
        nr = 0;
        for (j = 0; j < m; j++){
            if ( v[i][j] != 0)
                nr++;
        }
            if (max < nr)
                max=nr;
    }
    //aflam numarul liniei/liniilor care contine/contin numarul maxim de numere nenule//
    for (i = 0; i < n; i++) {
        nr = 0;
        for (j = 0; j < n; j++){
            if (v[i][j] != 0)
                nr++;
         }
         linia = i+1;
         if (max == nr)
            printf("Linia/Liniile cu cele mai multe elemente nenule: %d \n", linia);
     }
     return 0;
}

